## Restler 3.0 boilerplate with eloquent ORM 
This repository contains simple boilerplate to get started with restler having the Laravel Eloquent ORM pre installed.

#### Getting started
1. Download the boilerplate from the git repo.
2. Run composer install.
3. Now, you are all set to go.

#### Creating APIs
1. Create a class file in app/ folder same like Home.php.
2. Make an entry into the public/index.php for it.
3. You can also view Home.php on how to use eloquent ORM.
4. That's it.

