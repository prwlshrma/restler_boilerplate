<?php

require_once '../vendor/autoload.php';
use Luracast\Restler\Restler;
use Illuminate\Database\Capsule\Manager as DBO;

$dbo = new DBO();

$dbo->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'database_name',
    'username'  => 'root',
    'password'  => 'password',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Make this Capsule instance available globally via static methods
$dbo->setAsGlobal();

// Setup the Eloquent ORM
$dbo->bootEloquent();

$r = new Restler();
$r->addAPIClass('Luracast\\Restler\\Resources'); //this creates resources.json at API Root
$r->setSupportedFormats('JsonFormat', 'XmlFormat');

$r->addAPIClass('Home','');
$r->handle();



