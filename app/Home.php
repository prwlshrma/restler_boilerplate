<?php
use Illuminate\Database\Capsule\Manager as DBO;
class Home {
    /**
     * 
     * @url \
     */
    public function index() {
        return "Restler Demo API 1.0";
    }
    /**
     * 
     * @url GET eloquent
     */
    public function eloquentTest($page=1,$pagesize=10) {
        $offset = $pagesize*($page-1);
        $products = DBO::table('posts')->limit($pagesize)->offset($offset)->get();
        return $products;
    }
}
